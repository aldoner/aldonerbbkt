function loginUsuario() {
  var txtUsuario = document.getElementById("txtUsuario");
  var txtClave = document.getElementById("txtClave");
  var msjLogin = document.getElementById("msjLogin");
  var usuario = txtUsuario.value;
  var claveIngresada = txtClave.value;
  if(usuario == '')
	  msjLogin.innerText = "Debe ingresar un ID de usuario.";
  else
	if(claveIngresada  == '')
		msjLogin.innerText = "Debe ingresar una clave para el usuario.";
	else
	{
		var claveRegistrada = localStorage.getItem(usuario);
		if(claveRegistrada == null)
			msjLogin.innerText = "Usuario no existe.";
		else
			if(claveIngresada != claveRegistrada)
				msjLogin.innerText = "Clave incorrecta.";
			else
				msjLogin.innerText = "Login Ok.";
	}
}
function registrarUsuario() {
  var txtUsuario = document.getElementById("txtUsuario");
  var txtClave = document.getElementById("txtClave");
  var msjLogin = document.getElementById("msjLogin");
  var usuario = txtUsuario.value;
  var claveIngresada = txtClave.value;
  if(usuario == '')
	  msjLogin.innerText = "Debe ingresar un ID de usuario.";
  else
	if(claveIngresada  == '')
		msjLogin.innerText = "Debe ingresar una clave para el usuario.";
	else
	{
		var claveRegistrada = localStorage.getItem(usuario);
		localStorage.setItem(usuario,claveIngresada);
		if(claveRegistrada == null)
			msjLogin.innerText = "Cuenta registrado correctamente.";
		else
			msjLogin.innerText = "Clave modificada correctamente.";
	}
}
function removeUsuario() {
  var txtUsuario = document.getElementById("txtUsuario");
  var msjLogin = document.getElementById("msjLogin");
  var usuario = txtUsuario.value;
  if(usuario == '')
	  msjLogin.innerText = "Debe ingresar un ID de usuario.";
  else
  {
	  var claveRegistrada = localStorage.getItem(usuario)
	  if(claveRegistrada == null)
			msjLogin.innerText = "Usuario no existe.";
		else
		{
			localStorage.removeItem(usuario);
			msjLogin.innerText = "Cuenta dada de baja correctamente.";
		}
  }
}
function clearUsuarios() {
  var msjLogin = document.getElementById("msjLogin");
  var nroCtas = localStorage.length;
  localStorage.clear();
  if(nroCtas > 0)
	  msjLogin.innerText = "Se eliminaron " + nroCtas + " cuentas";
  else
	  msjLogin.innerText = "No se encontraron cuentas a eliminar.";
}
function loginSUsuario() {
  var txtUsuario = document.getElementById("txtUsuario");
  var txtClave = document.getElementById("txtClave");
  var msjLogin = document.getElementById("msjLogin");
  var usuario = txtUsuario.value;
  var claveIngresada = txtClave.value;
  if(usuario == '')
	  msjLogin.innerText = "Debe ingresar un ID de usuario.";
  else
	if(claveIngresada  == '')
		msjLogin.innerText = "Debe ingresar una clave para el usuario.";
	else
	{
		var claveRegistrada = sessionStorage.getItem(usuario);
		if(claveRegistrada == null)
			msjLogin.innerText = "Usuario no existe.";
		else
			if(claveIngresada != claveRegistrada)
				msjLogin.innerText = "Clave incorrecta.";
			else
				msjLogin.innerText = "Login Ok.";
	}
}
function registrarSUsuario() {
  var txtUsuario = document.getElementById("txtUsuario");
  var txtClave = document.getElementById("txtClave");
  var msjLogin = document.getElementById("msjLogin");
  var usuario = txtUsuario.value;
  var claveIngresada = txtClave.value;
  if(usuario == '')
	  msjLogin.innerText = "Debe ingresar un ID de usuario.";
  else
	if(claveIngresada  == '')
		msjLogin.innerText = "Debe ingresar una clave para el usuario.";
	else
	{
		var claveRegistrada = sessionStorage.getItem(usuario);
		sessionStorage.setItem(usuario,claveIngresada);
		if(claveRegistrada == null)
			msjLogin.innerText = "Cuenta registrado correctamente.";
		else
			msjLogin.innerText = "Clave modificada correctamente.";
	}
}
function removeSUsuario() {
  var txtUsuario = document.getElementById("txtUsuario");
  var msjLogin = document.getElementById("msjLogin");
  var usuario = txtUsuario.value;
  if(usuario == '')
	  msjLogin.innerText = "Debe ingresar un ID de usuario.";
  else
  {
	  var claveRegistrada = sessionStorage.getItem(usuario)
	  if(claveRegistrada == null)
			msjLogin.innerText = "Usuario no existe.";
		else
		{
			sessionStorage.removeItem(usuario);
			msjLogin.innerText = "Cuenta dada de baja correctamente.";
		}
  }
}
function clearSUsuarios() {
  var msjLogin = document.getElementById("msjLogin");
  var nroCtas = sessionStorage.length;
  sessionStorage.clear();
  if(nroCtas > 0)
	  msjLogin.innerText = "Se eliminaron " + nroCtas + " cuentas";
  else
	  msjLogin.innerText = "No se encontraron cuentas a eliminar.";
}